#!/bin/bash
#
# This is a set of iperf3 tests with various MPTCP configurations.

#set -x

server=$1
port=$2
duration=$3
test_id=$4

IP6="fde4:8dba:82e1::c4"

set_congestion_control () {
    sudo sysctl -w net.ipv4.tcp_congestion_control=$1
}

echo "--------------------------------"
echo "MODE 1: Client-to-server traffic"
echo "--------------------------------"

baseopt=" -c $server  -t $duration --test-id $test_id -J"

opt="$baseopt --port $port"

set_congestion_control olia
iperf3 $opt           -T "single_subflow,default_iface,olia"

sleep 1
set_congestion_control cubic
iperf3 $opt           -T "single_subflow,default_iface,cubic"

second_iface=`awk '$1 == "second_iface"  { print $2 }' /sent_from_host/host_status`
ipv6_capable=`awk '$1 == "ipv6_capable"  { print $2 }' /sent_from_host/host_status`

if [[ "$second_iface" == "true" ]]; then

	sleep 1
	set_congestion_control olia
	iperf3 $opt  -m eth2  -T "single_subflow,second_iface,olia"

	sleep 1
	set_congestion_control cubic
	iperf3 $opt  -m eth2  -T "single_subflow,second_iface,cubic"

#	echo "multiple interface"
	sleep 1
	set_congestion_control olia
	iperf3 $opt  -m eth1,eth2 -T "multi-iface,olia"

	sleep 1
	set_congestion_control cubic
	iperf3 $opt  -m eth1,eth2 -T "multi-iface,cubic"

#	echo "multiple interface, roundrobin scheduler"
#	sleep 1
#	set_congestion_control olia
#	sudo iperf3 $opt -m eth1,eth2 --scheduler=roundrobin -T "multi-iface,roundrobin"
fi

if [[ "$ipv6_capable" == "true" ]]; then

#	echo "dual IPv4-IPv6"
	sleep 1
	set_congestion_control olia
	iperf3 $opt -m eth1,$IP6 -T "dual_IPv4-IPv6,olia"

	sleep 1
	set_congestion_control cubic
	iperf3 $opt -m eth1,$IP6 -T "dual_IPv4-IPv6,cubic"

#	echo "dual stack, roundrobin scheduler"
#	sleep 1
#	set_congestion_control olia
#	sudo iperf3 $opt -m eth1,$IP6 --scheduler=roundrobin -T "IPv4-IPv6,roundrobin"
fi

# echo "two subflows, default interface"
#sleep 1
#set_congestion_control olia
#iperf3 $opt  -m eth1,eth1 -T "two_subflows,olia"
#sleep 1
#set_congestion_control cubic
#iperf3 $opt  -m eth1,eth1 -T "two_subflows,cubic"



echo "-----------------------------------------"
echo "MODE 2: reverse: server-to-client traffic"
echo "-----------------------------------------"

sleep 1
iperf3 $opt  -R         -T "reverse,default_iface"

#rr_port=$((5209 + RANDOM % 2 ))

if [[ "$second_iface" == "true" ]]; then
	sleep 1
	iperf3 $opt  -R -m eth2 -T "reverse,second_iface"

#	echo "reverse, multiple interface"
	sleep 1
	iperf3 $opt -R -m eth1,eth2  -T "reverse,multi-iface"
#	iperf3 $baseopt -R -m eth1,eth2 --port $rr_port -T "reverse,multi-iface,roundrobin"
fi

if [[ "$ipv6_capable" == "true" ]]; then
# 	echo "reverse, dual IPv4-IPv6"
	sleep 1
	iperf3 $opt -R -m eth1,$IP6  -T "reverse,IPv4-IPv6"
#	sleep 1
#	iperf3 $baseopt -R -m eth1,$IP6 --port $rr_port -T "reverse,IPv4-IPv6,roundrobin"
fi

#echo "multiple subflow, single interface"
sleep 1
iperf3 $opt  -R -m eth1,eth1 -T "reverse,two_subflows"

